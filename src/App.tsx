import React from 'react';
import clsx from 'clsx';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { AppBar, Avatar, Box, Card, CardActionArea, CardActions, CardContent, CardMedia, Chip, Container, IconButton, ListItemAvatar, StepContent, StepLabel, Stepper, Toolbar, Typography } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Button from '@material-ui/core/Button';
import logo from './icon.jpg';
import List from '@material-ui/core/List';
import MailIcon from '@material-ui/icons/Mail';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import { DataGrid, ColDef, ValueGetterParams } from '@material-ui/data-grid';
import CircularProgress, { CircularProgressProps } from '@material-ui/core/CircularProgress';

import Brightness4OutlinedIcon from '@material-ui/icons/Brightness4Outlined';
import './App.scss';

import Timeline from '@material-ui/lab/Timeline/Timeline';
import TimelineConnector from '@material-ui/lab/TimelineConnector/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent/TimelineContent';
import TimelineDot from '@material-ui/lab/TimelineDot/TimelineDot';
import TimelineItem from '@material-ui/lab/TimelineItem/TimelineItem';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent/TimelineOppositeContent';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator/TimelineSeparator';
import FaceIcon from '@material-ui/icons/Face';
import DoneIcon from '@material-ui/icons/Done';
import FastfoodIcon from '@material-ui/icons/Fastfood';
import LaptopMacIcon from '@material-ui/icons/LaptopMac';
import HotelIcon from '@material-ui/icons/Hotel';
import RepeatIcon from '@material-ui/icons/Repeat';
import Step from '@material-ui/core/Step/Step';


function App() {

  type Anchor = 'top' | 'left' | 'bottom' | 'right';

  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });


  const toggleDrawer = (anchor: Anchor, open: boolean) => (
    event: React.KeyboardEvent | React.MouseEvent,
  ) => {
    if (
      event &&
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' ||
        (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor: Anchor) => (
    <div
      className={'top'}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        {['Home', 'Account', 'Reports'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );
  const columns: ColDef[] = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'firstName', headerName: 'First name', width: 130 },
    { field: 'lastName', headerName: 'Last name', width: 130 },
    {
      field: 'age',
      headerName: 'Age',
      type: 'number',
      width: 90,
    },
    {
      field: 'fullName',
      headerName: 'Full name',
      description: 'This column has a value getter and is not sortable.',
      sortable: false,
      width: 160,
      valueGetter: (params: ValueGetterParams) =>
        `${params.getValue('firstName') || ''} ${params.getValue('lastName') || ''}`,
    },
  ];

  const rows = [
    { id: 1, lastName: 'Snow', firstName: 'Jon', age: 35 },
    { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
    { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
    { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16 },
    { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null },
    { id: 6, lastName: 'Melisandre', firstName: null, age: 150 },
    { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
    { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36 },
    { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
  ];

  function getSteps() {
    return ['Computer', 'Monitor', 'Keyboard'];
  }
  
  function getStepContent(step: number) {
    switch (step) {
      case 0:
        return `Dell XPS - intel i5 4690k - NVIDIA GTX 3090`;
      case 1:
        return 'LG';
      case 2:
        return `Duckey`;
      default:
        return 'Unknown step';
    }
  }
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };
  function CircularProgressWithLabel(props: CircularProgressProps & { value: number }) {
    return (
      <Box position="relative" display="inline-flex">
        <CircularProgress variant="static" {...props} />
        <Box
          top={0}
          left={0}
          bottom={0}
          right={0}
          position="absolute"
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <Typography variant="caption" component="div" color="textSecondary">{`${Math.round(
            props.value,
          )}%`}</Typography>
        </Box>
      </Box>
    );
  }

  

  return (
    <div className="App">
      <header className="App-header">
        <AppBar position="static">
          <Container maxWidth="lg" className="header-container">

            <div className="logo">
              <Typography variant="h1" className="site-title">
                M
              </Typography>
            </div>
            <div className="btns">
             <Button onClick={toggleDrawer('top', true)}><Brightness4OutlinedIcon className="theme-button"/></Button>
             <Button onClick={toggleDrawer('top', true)}><MenuOpenIcon className="menu-button"/></Button>
            </div>

            </Container>
        </AppBar>
      </header>
      <nav className="App-navigation">
        <Container maxWidth="lg">
          <React.Fragment key='top'>
            <SwipeableDrawer
              anchor='top'
              open={state['top']}
              onClose={toggleDrawer('top', false)}
              onOpen={toggleDrawer('top', true)}
            >
              {list('top')}
            </SwipeableDrawer>
          </React.Fragment>
        </Container>
      </nav>

      <main className="content">
        <Container maxWidth="lg">

          <div className="section-title">
              <Typography variant="h3" component="h3" className="site-title">
                Welcome to BITS
              </Typography>
              <Button variant="contained" color="primary">
                    Admin
                </Button>
            </div>

            <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={3}>
              <Grid item xs={12} sm={6} lg={4}>
                <Card className="cat-1" elevation={5}>
                  <CardActionArea>
                    <CardMedia className="cat-bg">
                      <Typography gutterBottom variant="h1" component="h2" className="cat-ill cat-1-ill-2">
                        B I T
                      </Typography>
                     
                    </CardMedia>
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                                          View Employee Table
                      </Typography>
                      <Typography variant="body2" color="textSecondary" component="p">
                                          Active Tab
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions>
                  <Button variant="contained" color="primary">
                                      Full Table
                  </Button>
                  <Button variant="contained" color="secondary">
                                      Search
                  </Button>
                  </CardActions>
                </Card>
              </Grid>
              <Grid item xs={12} sm={6} lg={4}>
              <Card className="cat-2" elevation={6}>
                  <CardActionArea>
                    <CardMedia className="cat-bg">
                      <Typography gutterBottom variant="h5" component="h2" className="cat-ill cat-1-ill-1">
                        D
                      </Typography>
                      <Typography gutterBottom variant="h5" component="h2" className="cat-ill cat-1-ill-2">
                        e
                      </Typography>
                      <Typography gutterBottom variant="h5" component="h2" className="cat-ill cat-1-ill-3">
                        f
                      </Typography>
                    </CardMedia>
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                                          Active Tickets
                      </Typography>
                      <Typography variant="body2" color="textSecondary" component="p">
                                         Open ticket Panel
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions>
                  <Button variant="contained" color="primary">
                    Critical 
                  </Button>
                  <Button variant="contained" color="secondary">
                  All Tickets
                  </Button>
                  </CardActions>
                </Card>
              </Grid>
              <Grid item xs={12} sm={6} lg={4}>
              <Card className="cat-3" elevation={5}>
                  <CardActionArea>
                    <CardMedia className="cat-bg">
                      <Typography gutterBottom variant="h5" component="h2" className="cat-ill cat-1-ill-1">
                        g
                      </Typography>
                      <Typography gutterBottom variant="h5" component="h2" className="cat-ill cat-1-ill-2">
                        H
                      </Typography>
                      <Typography gutterBottom variant="h5" component="h2" className="cat-ill cat-1-ill-3">
                        i
                      </Typography>
                    </CardMedia>
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                                          Check block chain
                      </Typography>
                      <Typography variant="body2" color="textSecondary" component="p">
                        validate the block chain
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions>
                  <Button variant="contained" color="primary">
                    Validate
                  </Button>
                  
                  </CardActions>
                </Card>
              </Grid>
              </Grid>
          </Container>
          </main>

      <section className="timeline">  
        <Container maxWidth="lg">
          <div style={{ height: 400, width: '100%' }}>
            <DataGrid rows={rows} columns={columns} pageSize={5} checkboxSelection />
          </div>
        </Container>
      </section>

      <section className="blocks">  
        <Container maxWidth="lg">


          <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={3}>
          
          <Grid item lg={4} className="timelineitems">

            <Paper elevation={4} className="timeline-title-paper">
              <Typography variant="h5" component="h5" className="timeline-title" align="center">
                Shipping Tracker
              </Typography>


            </Paper>
            

              <Timeline align="alternate">
                <TimelineItem>
                  <TimelineSeparator>
                    <TimelineDot variant="outlined" />
                    <TimelineConnector />
                  </TimelineSeparator>
                  <TimelineContent>Recived by shipper</TimelineContent>
                </TimelineItem>
                <TimelineItem>
                  <TimelineSeparator>
                    <TimelineDot variant="outlined" color="primary" />
                    <TimelineConnector />
                  </TimelineSeparator>
                  <TimelineContent>Processed</TimelineContent>
                </TimelineItem>
                <TimelineItem>
                  <TimelineSeparator>
                    <TimelineDot variant="outlined" color="secondary" />
                    <TimelineConnector />
                  </TimelineSeparator>
                  <TimelineContent>Out for delivery</TimelineContent>
                </TimelineItem>
                <TimelineItem>
                  <TimelineSeparator>
                    <TimelineDot variant="outlined" />
                  </TimelineSeparator>
                  <TimelineContent>Item delivered</TimelineContent>
                </TimelineItem>
              </Timeline>



            </Grid>
          
           <Grid item lg={8}>
              <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={3}>
                <Grid item lg={4}>
                  <Paper className="block-diagram-block" elevation={4} >
                    <Typography variant="h5" component="h5" align="center">
                      Jon Snow
                    </Typography>
                  </Paper>
                </Grid>
                <Grid item lg={4}>
                  <Paper className="block-diagram-block" elevation={4} >
                    <Typography variant="h5" component="h5" align="center">
                      Display personal data
                    </Typography>
                  </Paper>
                </Grid>
                <Grid item lg={4}>
                  <Paper className="block-diagram-block" elevation={4} >
                    <Typography variant="h5" component="h5" align="center">
                      Tickets
                    </Typography>
                  </Paper>
                </Grid>
                <Grid item lg={12}>

                <div className="steps">
            <Stepper activeStep={activeStep} orientation="vertical">
              {steps.map((label, index) => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                  <StepContent>
                    <Typography>{getStepContent(index)}</Typography>
                    <div className="steps-container">
                      <div>
                        <Button
                          disabled={activeStep === 0}
                          onClick={handleBack}
                          className="steps-btn">
                        
                          Back
                        </Button>
                        <Button
                          variant="contained"
                          color="primary"
                          onClick={handleNext}
                          className="steps-btn">
                        
                          {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                        </Button>
                      </div>
                    </div>
                  </StepContent>
                </Step>
              ))}
            </Stepper>
            {activeStep === steps.length && (
              <Paper square elevation={0} className="read-container">
                <Typography>All steps completed - you&apos;re finished</Typography>
                <Button onClick={handleReset} className="read-btn">
                  Reset
                </Button>
              </Paper>
            )}
          </div>

</Grid>

              </Grid>

            </Grid>
           
          </Grid>



        </Container>
        



      </section>
  
      <footer className="App-Sub-footer">
        <Container maxWidth="lg" className="footer-container">
          <ul>
            <li>Link 1</li>
            <li>Link 2</li>
            <li>Link 3</li>
          </ul>
        </Container>
      </footer>
      <footer className="App-footer">
        <Container maxWidth="lg" className="footer-container">
          <p><span className="bold-span">Asset Manager</span> by Bit::Shifters</p>
          <p>Copyright 2020</p>
        </Container>
      </footer>

    </div>


  );
}

export default App;
